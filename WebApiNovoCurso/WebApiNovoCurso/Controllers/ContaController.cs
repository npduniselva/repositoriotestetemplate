﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiNovoCurso.DAO;

namespace WebApiNovoCurso.Controllers
{
    public class ContaController : ApiController
    {
        private static InscricaoEntities contexto = new InscricaoEntities();
        private static Conta_DAO _repository = new Conta_DAO(contexto);
        
        public static List<tbCONTA> ListaPorId()
        {
            var query = contexto.tbCONTA.OrderBy(c => c.BOL_CODIGO).ToList();
            return query;
        }

        public static tbCONTA PegaPorId(Int32 _Id)
        {
            try
            {
                return _repository.AchePorCondicao(c => c.BOL_CODIGO == _Id).First();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string Inclui(tbCONTA registro)
        {
            string resultado = "OK";
            if (resultado == "OK")
            {
                try
                {
                    _repository.Inclui(registro);
                    _repository.Salva();
                }
                catch (Exception ex)
                {
                    resultado = "Erro ao adicionar registro!";
                }
            }
            return resultado;
        }


        public static string Atualiza(tbCONTA registro)
        {
            string resultado = "OK";
            if (resultado == "OK")
            {
                try
                {
                    var reg = _repository.AchePorCondicao(e => e.BOL_CODIGO == registro.BOL_CODIGO).First();
                    reg = registro;
                    _repository.Atualiza(reg);
                    _repository.Salva();
                }
                catch (Exception ex)
                {
                    resultado = "Erro ao atualizar registro!";
                }
            }
            return resultado;
        }

        public static string Exclui(Int32 _id)
        {
            string resultado = "OK";
            tbCONTA registro = PegaPorId(_id);
            if (registro != null)
            {
                String _EntitySetName = registro.EntityKey.EntitySetName;
                try
                {
                    _repository.Exclui(registro);
                    _repository.Salva();
                    resultado = "OK";
                }
                catch (Exception ex)
                {
                    resultado = "Erro ao excluir registro!";
                }
            }
            else
            {
                resultado = "Registro não encontrado!";
            }
            return resultado;
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                contexto?.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
