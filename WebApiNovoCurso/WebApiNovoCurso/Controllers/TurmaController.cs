﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiNovoCurso.DAO;

namespace WebApiNovoCurso.Controllers
{
    public class TurmaController : ApiController
    {
        private static InscricaoEntities contexto = new InscricaoEntities();
        private static Turma_DAO _repository = new Turma_DAO(contexto);
        
        public static List<tbTURMA> ListaPorId()
        {
            var query = contexto.tbTURMA.OrderBy(c => c.TRM_CODIGO).ToList();
            return query;
        }

        public static tbTURMA PegaPorId(Int32 _Id)
        {
            try
            {
                return _repository.AchePorCondicao(c => c.TRM_CODIGO == _Id).First();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string Inclui(tbTURMA registro)
        {
            string resultado = "OK";
            if (resultado == "OK")
            {
                try
                {
                    _repository.Inclui(registro);
                    _repository.Salva();
                }
                catch (Exception ex)
                {
                    resultado = "Erro ao adicionar registro!";
                }
            }
            return resultado;
        }


        public static string Atualiza(tbTURMA registro)
        {
            string resultado = "OK";
            if (resultado == "OK")
            {
                try
                {
                    var reg = _repository.AchePorCondicao(e => e.TRM_CODIGO == registro.TRM_CODIGO).First();
                    reg = registro;
                    _repository.Atualiza(reg);
                    _repository.Salva();
                }
                catch (Exception ex)
                {
                    resultado = "Erro ao atualizar registro!";
                }
            }
            return resultado;
        }

        public static string Exclui(Int32 _id)
        {
            string resultado = "OK";
            tbTURMA registro = PegaPorId(_id);
            if (registro != null)
            {
                String _EntitySetName = registro.EntityKey.EntitySetName;
                try
                {
                    _repository.Exclui(registro);
                    _repository.Salva();
                    resultado = "OK";
                }
                catch (Exception ex)
                {
                    resultado = "Erro ao excluir registro!";
                }
            }
            else
            {
                resultado = "Registro não encontrado!";
            }
            return resultado;
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                contexto?.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
