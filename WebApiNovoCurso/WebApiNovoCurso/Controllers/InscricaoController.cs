﻿using iugu.net.Entity;
using iugu.net.Lib;
using iugu.net.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using WebApiNovoCurso.DAO;
using WebApiNovoCurso.Models;
using Newtonsoft.Json;

namespace WebApiNovoCurso.Controllers
{
    public class InscricaoController : ApiController
    {
        private static InscricaoEntities contexto = new InscricaoEntities();
        private static Inscricao_DAO _repository = new Inscricao_DAO(contexto);

        //Cria lista de inscritos
        private static List<Inscricao> inscritos = new List<Inscricao>();
        
        public async Task<IHttpActionResult> Get(Int32 codturma)
        {
            using (var context = new InscricaoEntities())
            {
                var total = context.tbINSCRICAO.Where(c => c.TRM_CODIGO == codturma).Count();
                var list = context.tbINSCRICAO.Where(c => c.TRM_CODIGO == codturma).OrderByDescending(c => c.TRM_PARCELA1_VENCTO)
                    .Select(a => new {
                        a.INS_CODIGO,
                        a.INS_INSCRICAO,
                        a.TRM_CODIGO,
                        a.INS_NOME,
                        a.INS_SEXO,
                        a.INS_NASCIMENTO,
                        a.INS_CPF,
                        a.INS_RG_IDENTIDADE,
                        a.INS_RG_ORGAO,
                        a.INS_RG_UF,
                        a.INS_END_RUA,
                        a.INS_END_NUMERO,
                        a.INS_END_FONE,
                        a.INS_END_BAIRRO,
                        a.INS_END_CEP,
                        a.INS_END_CIDADE,
                        a.INS_END_UF,
                        a.INS_DATA_INSCRICAO,
                        a.INS_NOSSO_NUMERO1,
                        a.TRM_PARCELA1_VENCTO,
                        a.TRM_PARCELA1_VALOR,
                        a.INS_EMAIL
                    }).ToList();
                return  Ok(new { total, list });  //Ok(new { total, list });
            }
        }

        //Faz inserção de dados [FromBody]
        public async void Post(string codturma, string nome, string telefone, string email, string sexo, string nascimento, string cpf, string rg, string rgorgao, string rguf, string rua, string numero, string bairro, string cep, string cidade, string uf)
        {
            string resultado = "";

            if (!string.IsNullOrEmpty(codturma) && !string.IsNullOrEmpty(nome) && !string.IsNullOrEmpty(telefone) && !string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(sexo) && !string.IsNullOrEmpty(nascimento) && !string.IsNullOrEmpty(cpf) && !string.IsNullOrEmpty(rg) && !string.IsNullOrEmpty(rgorgao) && !string.IsNullOrEmpty(rguf) && !string.IsNullOrEmpty(rua) && !string.IsNullOrEmpty(numero) && !string.IsNullOrEmpty(bairro) && !string.IsNullOrEmpty(cep) && !string.IsNullOrEmpty(cidade) && !string.IsNullOrEmpty(uf)) //Inclui em banco
            {
                tbTURMA turma = TurmaController.PegaPorId(Convert.ToInt32(codturma));
                if (turma != null)
                {
                    tbCURSO curso = CursoController.PegaPorId(Convert.ToInt32(turma.CUR_CODIGO));                    
                    tbCONTA conta = ContaController.PegaPorId(Convert.ToInt32(turma.BOL_CODIGO));

                    tbINSCRICAO registro = new tbINSCRICAO();
                    registro.TRM_CODIGO = Convert.ToInt32(codturma);
                    registro.INS_NOME = nome;
                    registro.INS_SEXO = sexo;
                    registro.INS_NASCIMENTO = Convert.ToDateTime(nascimento);
                    registro.INS_CPF = cpf;
                    registro.INS_RG_IDENTIDADE = rg;
                    registro.INS_RG_ORGAO = rgorgao;
                    registro.INS_RG_UF = rguf;
                    registro.INS_END_RUA = rua;
                    registro.INS_END_NUMERO = numero;
                    registro.INS_END_FONE = telefone;
                    registro.INS_END_BAIRRO = bairro;
                    registro.INS_END_CEP = cep;
                    registro.INS_END_CIDADE = cidade;
                    registro.INS_END_UF = uf;
                    registro.INS_DATA_INSCRICAO = DateTime.Now.ToString().Substring(0, 10);
                    registro.TRM_PARCELA1_VENCTO = DateTime.Now.AddDays(2);
                    registro.TRM_PARCELA1_VALOR = Convert.ToDecimal(turma.TRM_PARCELA1_VALOR);
                    registro.INS_EMAIL = email;

                    registro.INS_INSCRICAO = 0;
                    registro.INS_NOSSO_NUMERO1 = "";

                    resultado = Inclui(registro);
                    if (resultado == "OK")
                    {
                        tbINSCRICAO novoRegistro = PegaUltimoPorTurmaeNome(Convert.ToInt32(registro.TRM_CODIGO), registro.INS_NOME.ToString());
                        if (novoRegistro != null)
                        {
                            novoRegistro.INS_INSCRICAO = Convert.ToDouble(novoRegistro.TRM_CODIGO.ToString() + novoRegistro.INS_CODIGO.ToString());
                            novoRegistro.INS_NOSSO_NUMERO1 = "14" + novoRegistro.TRM_CODIGO.ToString() + novoRegistro.INS_CODIGO.ToString() + "1";
                            resultado = Atualiza(novoRegistro);

                            if (resultado == "OK")
                            {
                                var invoiceRequest = new InvoiceRequestMessage(registro.INS_EMAIL, Convert.ToDateTime(registro.TRM_PARCELA1_VENCTO),
                 new[]
                 {
                                            new Item
                                            {
                                                description = $@"{"Inscrição"} - {registro.INS_INSCRICAO} - {turma.TRM_CODIGO + " " + curso.CUR_DESCRICAO }",
                                                price_cents = (int) (registro.TRM_PARCELA1_VALOR.Value * 100),
                                                quantity = 1
                                            }
                 })
                                {
                                    Payer = new PayerModel
                                    {
                                        Email = registro.INS_EMAIL,
                                        Name = registro.INS_NOME,
                                        Address = new AddressModel
                                        {
                                            ZipCode = registro.INS_END_CEP,
                                            Number = registro.INS_END_NUMERO,
                                            State = registro.INS_END_UF,
                                            City = registro.INS_END_CIDADE,
                                            Street = registro.INS_END_RUA,
                                            District = registro.INS_END_BAIRRO
                                        },
                                        CpfOrCnpj = registro.INS_CPF,
                                        Phone = registro.INS_END_FONE
                                    },
                                    PaymentMethod = conta.TipoPagamento //all conta.TipoPagamento
                                };

                                using (var apiInvoice = new Invoice())
                                {
                                    try
                                    {
#if DEBUG
                                        //var result = apiInvoice.CreateAsync(invoiceRequest, conta.IuguTestApiToken);
                                        var result = await apiInvoice.CreateAsync(invoiceRequest, conta.IuguTestApiToken).ConfigureAwait(false);
#else
                                    var result = await apiInvoice.CreateAsync(invoiceRequest, conta.IuguLiveApiToken).ConfigureAwait(false);
#endif
                                        if (result != null)
                                        {
                                            var objeto = result.id;
                                            
                                            tbFatura fatura = new tbFatura();
                                            fatura.Id = result.id;
                                            fatura.IdInscricao = novoRegistro.INS_CODIGO;
                                            fatura.Status = result.status.ToString();
                                            fatura.NumeroParcelas = 0;
                                            fatura.Metodo = invoiceRequest.PaymentMethod;
                                            resultado = FaturaController.Inclui(fatura);
                                            
                                            if (resultado == "OK")
                                            {
                                                Redirect(result.secure_url);
                                            }

                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        throw;
                                    }

                                }
                            }
                            else
                            {
                                ModelState.AddModelError("", "Erro ao atualizar registro");
                            }
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Erro ao incluir registro");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Turma não existe");
                }
                
            }
            else
            {
                ModelState.AddModelError("", "Insira os parametros corretamente");
            }

            Dispose();
        }

        public string Get(string idfatura)
        {
            tbFatura fatura = FaturaController.PegaPorId(idfatura);
            tbINSCRICAO insc = PegaPorId(Convert.ToInt32(fatura.IdInscricao));
            tbTURMA turma = TurmaController.PegaPorId(Convert.ToInt32(insc.TRM_CODIGO));
            tbCURSO curso = CursoController.PegaPorId(Convert.ToInt32(turma.CUR_CODIGO)); 
            tbCONTA conta = ContaController.PegaPorId(Convert.ToInt32(turma.BOL_CODIGO));

            using (var invoiceClient = new Invoice())
            {
#if DEBUG
                var invoice = invoiceClient.GetAsync(idfatura, conta.IuguTestApiToken).ConfigureAwait(false);
#else
                var invoice = invoiceClient.GetAsync(idfatura, conta.IuguLiveApiToken).ConfigureAwait(false);
#endif

                return invoice.ToString();
            }
        }


//        public async Task<JsonResult> BuscarFaturas(string id)
//        {
//            //var sa = new JsonSerializerSettings();

//            if (string.IsNullOrEmpty(id))
//                return Json(new { },sa);

//            var conta =  contexto.tbCONTA.FirstOrDefault(x => x.IuguSubcontaAccountId == id);

//            using (var invoice = new Invoice())
//            {
//#if DEBUG

//                var invoices = await invoice.GetAllAsync(conta.IuguTestApiToken).ConfigureAwait(false);
//#else
//                var invoices = await invoice.GetAllAsync(conta.IuguLiveApiToken).ConfigureAwait(false);
//#endif
//                var items = invoices.items;
//                var itemsId = items.Select(x => x.id).ToList();
//                var faturas = contexto.tbFatura.Where(x => itemsId.Contains(x.Id)).ToList();
//                foreach (var i in items)
//                {
//                    var f = faturas.FirstOrDefault(x => x.Id == i.id);
//                    if (f != null)
//                    {
//                        var payiedWith = i.variables.FirstOrDefault(x => x.variable == "payment_method");
//                        f.Metodo = payiedWith?.value;
//                        int.TryParse(i.installments?.ToString() ?? "0", out var numero);
//                        f.NumeroParcelas = numero;
//                        f.Status = i.status;
//                        f.DataPagamento = i.paid_at?.ToString();
//                        decimal.TryParse(i.total_paid.Replace("R$", "").Trim(), out var valorPago);
//                        f.ValorPago = valorPago;
//                        //contexto.Entry(f).State = EntityState.Modified;
//                    }
//                }

//                 contexto.SaveChanges();
//                var inscricaoIds = faturas.Select(x => x.IdInscricao).ToList();
//                var inscritos = contexto.tbINSCRICAO.Where(x => inscricaoIds.Contains(x.INS_CODIGO)).ToList();
//                dynamic faturaInscricao;
//                if (inscritos.Any())
//                    faturaInscricao = (from _i in items
//                                       join _f in faturas.DefaultIfEmpty() on _i.id equals _f?.Id
//                                       join _ins in inscritos.DefaultIfEmpty() on _f.IdInscricao equals _ins?.INS_CODIGO
//                                       select new { _i.id, _i.due_date, _i.status, _i.secure_url, nome = _ins?.INS_NOME, cpf = _ins?.INS_CPF, _i.total_paid }).ToList();
//                else
//                    faturaInscricao = items.Select(x => new { x.id, x.due_date, x.status, x.secure_url, x.total_paid }).ToList();

//                return Json(new { Total = faturaInscricao.Count, Rows = faturaInscricao }, sa);
//            }
//        }




        //Deleta por parâmetro
        public void Delete(string nome)
        {
            inscritos.RemoveAt(inscritos.IndexOf(inscritos.First(x => x.Nome.Equals(nome))));
        }


        public static List<tbINSCRICAO> Gride(string _cmpSort, string _desc)
        {
            var query = new List<tbINSCRICAO>();
            _cmpSort = _cmpSort == "" ? "Id" : _cmpSort;
            switch (_cmpSort)
            {
                case "Id":
                    query = contexto.tbINSCRICAO
                         .Where(c => _desc == null || c.INS_NOME.Contains(_desc))
                         .OrderBy(c => c.INS_CODIGO)
                         .ToList();
                    break;
            }
            return query;
        }

        public static List<tbINSCRICAO> ListaPorDescricao()
        {
            var query = contexto.tbINSCRICAO.OrderBy(c => c.INS_NOME).ToList();
            return query;
        }

        //[HttpGet]
        //public static List<tbINSCRICAO> ListaPorTurma(Int32 _codTurma)
        //{
        //    var query = contexto.tbINSCRICAO.Where(c => c.TRM_CODIGO == _codTurma).OrderBy(c => c.INS_NOME).ToList();
        //    return query;
        //}

        public static tbINSCRICAO PegaPorId(int _Id)
        {
            try
            {
                return _repository.AchePorCondicao(c => c.INS_CODIGO == _Id).First();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static tbINSCRICAO PegaUltimoPorTurmaeNome(int _turma, string _nome)
        {
            try
            {
                return _repository.AchePorCondicao(c => c.TRM_CODIGO == _turma && c.INS_NOME == _nome).OrderByDescending(x => x.TRM_PARCELA1_VENCTO).First();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string Inclui(tbINSCRICAO registro)
        {
            string resultado = "OK";
            if (resultado == "OK")
            {
                try
                {
                    _repository.Inclui(registro);
                    _repository.Salva();
                }
                catch (Exception ex)
                {
                    resultado = "Erro ao adicionar registro!";
                }
            }
            return resultado;
        }
        
        public static string Atualiza(tbINSCRICAO registro)
        {
            string resultado = "OK";
            if (resultado == "OK")
            {
                try
                {
                    var reg = _repository.AchePorCondicao(e => e.INS_CODIGO == registro.INS_CODIGO).First();
                    reg = registro;
                    _repository.Atualiza(reg);
                    _repository.Salva();
                }
                catch (Exception ex)
                {
                    resultado = "Erro ao atualizar registro!";
                }
            }
            return resultado;
        }

        public static string Exclui(int _id)
        {
            string resultado = "OK";
            tbINSCRICAO registro = PegaPorId(_id);
            if (registro != null)
            {
                String _EntitySetName = registro.EntityKey.EntitySetName;
                try
                {
                    _repository.Exclui(registro);
                    _repository.Salva();
                    resultado = "OK";
                }
                catch (Exception ex)
                {
                    resultado = "Erro ao excluir registro!";
                }
            }
            else
            {
                resultado = "Registro não encontrado!";
            }
            return resultado;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                contexto?.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
