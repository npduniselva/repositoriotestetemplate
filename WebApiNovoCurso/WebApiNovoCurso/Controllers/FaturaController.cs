﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiNovoCurso.DAO;

namespace WebApiNovoCurso.Controllers
{
    public class FaturaController : ApiController
    {
        private static InscricaoEntities contexto = new InscricaoEntities();
        private static Fatura_DAO _repository = new Fatura_DAO(contexto);
        
        public static List<tbFatura> ListaPorId()
        {
            var query = contexto.tbFatura.OrderBy(c => c.Id).ToList();
            return query;
        }

        public static tbFatura PegaPorId(string _Id)
        {
            try
            {
                return _repository.AchePorCondicao(c => c.Id == _Id).First();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string Inclui(tbFatura registro)
        {
            string resultado = "OK";
            if (resultado == "OK")
            {
                try
                {
                    _repository.Inclui(registro);
                    _repository.Salva();
                }
                catch (Exception ex)
                {
                    resultado = "Erro ao adicionar registro!";
                }
            }
            return resultado;
        }


        public static string Atualiza(tbFatura registro)
        {
            string resultado = "OK";
            if (resultado == "OK")
            {
                try
                {
                    var reg = _repository.AchePorCondicao(e => e.Id == registro.Id).First();
                    reg = registro;
                    _repository.Atualiza(reg);
                    _repository.Salva();
                }
                catch (Exception ex)
                {
                    resultado = "Erro ao atualizar registro!";
                }
            }
            return resultado;
        }

        public static string Exclui(string _id)
        {
            string resultado = "OK";
            tbFatura registro = PegaPorId(_id);
            if (registro != null)
            {
                String _EntitySetName = registro.EntityKey.EntitySetName;
                try
                {
                    _repository.Exclui(registro);
                    _repository.Salva();
                    resultado = "OK";
                }
                catch (Exception ex)
                {
                    resultado = "Erro ao excluir registro!";
                }
            }
            else
            {
                resultado = "Registro não encontrado!";
            }
            return resultado;
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                contexto?.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
