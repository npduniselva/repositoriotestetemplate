﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiNovoCurso.DAO;

namespace WebApiNovoCurso.Controllers
{
    public class CursoController : ApiController
    {
        private static InscricaoEntities contexto = new InscricaoEntities();
        private static Curso_DAO _repository = new Curso_DAO(contexto);
        
        public static List<tbCURSO> ListaPorId()
        {
            var query = contexto.tbCURSO.OrderBy(c => c.CUR_CODIGO).ToList();
            return query;
        }

        public static tbCURSO PegaPorId(Int32 _Id)
        {
            try
            {
                return _repository.AchePorCondicao(c => c.CUR_CODIGO == _Id).First();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string Inclui(tbCURSO registro)
        {
            string resultado = "OK";
            if (resultado == "OK")
            {
                try
                {
                    _repository.Inclui(registro);
                    _repository.Salva();
                }
                catch (Exception ex)
                {
                    resultado = "Erro ao adicionar registro!";
                }
            }
            return resultado;
        }


        public static string Atualiza(tbCURSO registro)
        {
            string resultado = "OK";
            if (resultado == "OK")
            {
                try
                {
                    var reg = _repository.AchePorCondicao(e => e.CUR_CODIGO == registro.CUR_CODIGO).First();
                    reg = registro;
                    _repository.Atualiza(reg);
                    _repository.Salva();
                }
                catch (Exception ex)
                {
                    resultado = "Erro ao atualizar registro!";
                }
            }
            return resultado;
        }

        public static string Exclui(Int32 _id)
        {
            string resultado = "OK";
            tbCURSO registro = PegaPorId(_id);
            if (registro != null)
            {
                String _EntitySetName = registro.EntityKey.EntitySetName;
                try
                {
                    _repository.Exclui(registro);
                    _repository.Salva();
                    resultado = "OK";
                }
                catch (Exception ex)
                {
                    resultado = "Erro ao excluir registro!";
                }
            }
            else
            {
                resultado = "Registro não encontrado!";
            }
            return resultado;
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                contexto?.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
