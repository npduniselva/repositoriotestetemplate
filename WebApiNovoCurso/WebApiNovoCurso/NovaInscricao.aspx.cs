﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApiNovoCurso.Controllers;

namespace WebApiNovoCurso
{
    public partial class NovaInscricao : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           string _codturma = (Request.QueryString["codturma"] != null ? Request.QueryString["codturma"] : "");
           string _nome = (Request.QueryString["nome"] != null ? Request.QueryString["nome"] : "");
           string _telefone = (Request.QueryString["telefone"] != null ? Request.QueryString["telefone"] : "");
           string _email = (Request.QueryString["email"] != null ? Request.QueryString["email"] : "");
           string _sexo = (Request.QueryString["sexo"] != null ? Request.QueryString["sexo"] : "");
           string _nascimento = (Request.QueryString["nascimento"] != null ? Request.QueryString["nascimento"] : "");
           string _cpf = (Request.QueryString["cpf"] != null ? Request.QueryString["cpf"] : "");
           string _rg = (Request.QueryString["rg"] != null ? Request.QueryString["rg"] : "");
           string _rgorgao = (Request.QueryString["rgorgao"] != null ? Request.QueryString["rgorgao"] : "");
           string _rguf = (Request.QueryString["rgorgao"] != null ? Request.QueryString["rgorgao"] : "");
           string _rua = (Request.QueryString["rua"] != null ? Request.QueryString["rua"] : "");
           string _numero = (Request.QueryString["numero"] != null ? Request.QueryString["numero"] : "");
           string _bairro = (Request.QueryString["bairro"] != null ? Request.QueryString["bairro"] : "");
           string _cep = (Request.QueryString["cep"] != null ? Request.QueryString["cep"] : "");
           string _cidade = (Request.QueryString["cidade"] != null ? Request.QueryString["cidade"] : "");
           string _uf = (Request.QueryString["uf"] != null ? Request.QueryString["uf"] : "");

            CarregaFrm(_codturma, _nome, _telefone, _email, _sexo, _nascimento, _cpf, _rg, _rgorgao, _rguf, _rua, _numero, _bairro, _cep, _cidade, _uf);
        }

        public void CarregaFrm(string codturma, string nome, string telefone, string email, string sexo, string nascimento, string cpf, string rg, string rgorgao, string rguf, string rua, string numero, string bairro, string cep, string cidade, string uf)
        {
            



            if(!string.IsNullOrEmpty(codturma) 
                && !string.IsNullOrEmpty( nome) 
                && !string.IsNullOrEmpty( telefone) 
                && !string.IsNullOrEmpty( email) 
                && !string.IsNullOrEmpty( sexo) 
                && !string.IsNullOrEmpty( nascimento) 
                && !string.IsNullOrEmpty( cpf) 
                && !string.IsNullOrEmpty( rg) 
                && !string.IsNullOrEmpty( rgorgao) 
                && !string.IsNullOrEmpty( rguf) 
                && !string.IsNullOrEmpty( rua) 
                && !string.IsNullOrEmpty( numero) 
                && !string.IsNullOrEmpty( bairro) 
                && !string.IsNullOrEmpty( cep) 
                && !string.IsNullOrEmpty( cidade) 
                && !string.IsNullOrEmpty(uf))
            {
                txtcodturma.Value = codturma;
                txtnome.Value = nome;
                txttelefone.Value = telefone;
                txtemail.Value = email;
                txtsexo.Value = sexo;
                txtnascimento.Value = nascimento;
                txtcpf.Value = cpf;
                txtrg.Value = rg;
                txtrgorgao.Value = rgorgao;
                txtrguf.Value = rguf;
                txtrua.Value = rua;
                txtnumero.Value = numero;
                txtbairro.Value = bairro;
                txtcep.Value = cep;
                txtcidade.Value = cidade;
                txtuf.Value = uf;
                //inscricao.Post(codturma, nome, telefone, email, sexo, nascimento, cpf, rg, rgorgao, rguf, rua, numero, bairro, cep, cidade, uf);
            }
            else
            {
                txtcodturma.Value = "Tá de sacanagem mermão";
            }
            
        }

        protected void butEnviar_Click(object sender, EventArgs e)
        {
            InscricaoController inscricao = new InscricaoController();
            inscricao.Post(txtcodturma.Value, txtnome.Value, txttelefone.Value, txtemail.Value, txtsexo.Value, txtnascimento.Value, txtcpf.Value, txtrg.Value, txtrgorgao.Value, txtrguf.Value, txtrua.Value, txtnumero.Value, txtbairro.Value, txtcep.Value, txtcidade.Value, txtuf.Value);
        }
    }
}