﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using WebApiNovoCurso.Interface;

namespace WebApiNovoCurso.DAO
{
    public abstract class Abstract_DAO<T> : IDao<T> where T : class
    {
        InscricaoEntities Entidades = new InscricaoEntities();        

        #region IDao<T> Members


        public void Inclui(T pEntity)
        {
            Entidades.AddObject(pEntity.GetType().Name, pEntity);
        }

        public void Exclui(T pEntity)
        {
            Entidades.DeleteObject(pEntity);
        }

        public void Atacha(T pEntity)
        {
            Entidades.AttachTo(pEntity.GetType().Name, pEntity);
        }

        public void DesAtacha(T pEntity)
        {
            Entidades.Detach(pEntity);
        }

        public void Atualiza(T pEntity)
        {
            Entidades.ApplyCurrentValues<T>(pEntity.GetType().Name, pEntity);

        }

        public List<T> AchePorCondicao(Expression<Func<T, bool>> Condicao)
        {
            return Entidades.CreateObjectSet<T>().Where(Condicao).ToList();

        }

        public List<T> PegaTudo()
        {

            return Entidades.CreateObjectSet<T>().ToList();

        }

        public void Salva()
        {
            Entidades.SaveChanges();
        }
        public void Salva(T pEntity)
        {

            Entidades.Connection.Open();
            var transaction = Entidades.Connection.BeginTransaction();

            try
            {
                Entidades.SaveChanges();
                transaction.Commit();

            }
            catch (Exception ex)
            {
                if (Entidades.ObjectStateManager.GetObjectStateEntry(pEntity).State == EntityState.Deleted || Entidades.ObjectStateManager.GetObjectStateEntry(pEntity).State == EntityState.Modified)
                    Entidades.Refresh(RefreshMode.StoreWins, pEntity);
                else if (Entidades.ObjectStateManager.GetObjectStateEntry(pEntity).State == EntityState.Added)
                    Entidades.Detach(pEntity);
                Entidades.AcceptAllChanges();
                transaction.Rollback();
                transaction.Dispose();
                Entidades.Connection.Close();
                throw ex;
            }
            Entidades.Connection.Close();

        }

        public int Qtde()
        {
            return Entidades.CreateObjectSet<T>().Count();
        }

        #endregion
    }
}