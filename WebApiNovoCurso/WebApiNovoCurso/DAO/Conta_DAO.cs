﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiNovoCurso.Interface;

namespace WebApiNovoCurso.DAO
{
    public class Conta_DAO : Abstract_DAO<tbCONTA>, IConta
    {
        private object _context = new InscricaoEntities();

        public Conta_DAO(InscricaoEntities context)
        {
            _context = context;
        }
    }
}