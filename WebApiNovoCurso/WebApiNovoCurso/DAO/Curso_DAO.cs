﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiNovoCurso.Interface;

namespace WebApiNovoCurso.DAO
{
    public class Curso_DAO : Abstract_DAO<tbCURSO>, ICurso
    {
        private object _context = new InscricaoEntities();

        public Curso_DAO(InscricaoEntities context)
        {
            _context = context;
        }
    }
}