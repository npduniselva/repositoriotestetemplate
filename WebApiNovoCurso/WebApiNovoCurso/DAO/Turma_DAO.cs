﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiNovoCurso.Interface;

namespace WebApiNovoCurso.DAO
{
    public class Turma_DAO : Abstract_DAO<tbTURMA>, ITurma
    {
        private object _context = new InscricaoEntities();

        public Turma_DAO(InscricaoEntities context)
        {
            _context = context;
        }
    }
}