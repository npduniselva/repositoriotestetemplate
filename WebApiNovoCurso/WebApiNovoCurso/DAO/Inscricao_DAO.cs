﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiNovoCurso.Interface;

namespace WebApiNovoCurso.DAO
{
    public class Inscricao_DAO : Abstract_DAO<tbINSCRICAO>, IInscricao
    {
        private object _context = new InscricaoEntities();

        public Inscricao_DAO(InscricaoEntities context)
        {
            _context = context;
        }
    }
}