﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiNovoCurso.Interface;

namespace WebApiNovoCurso.DAO
{
    public class TesteInscricaoExterna_DAO : Abstract_DAO<tbTesteInscricaoExterna>, ITesteInscricaoExterna
    {
        private object _context = new InscricaoEntities();

        public TesteInscricaoExterna_DAO(InscricaoEntities context)
        {
            _context = context;
        }
    }
}