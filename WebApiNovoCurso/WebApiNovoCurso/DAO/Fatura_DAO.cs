﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiNovoCurso.Interface;

namespace WebApiNovoCurso.DAO
{
    public class Fatura_DAO : Abstract_DAO<tbFatura>, IFatura
    {
        private object _context = new InscricaoEntities();

        public Fatura_DAO(InscricaoEntities context)
        {
            _context = context;
        }
    }
}