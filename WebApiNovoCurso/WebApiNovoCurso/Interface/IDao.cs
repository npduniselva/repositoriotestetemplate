﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace WebApiNovoCurso.Interface
{
    public interface IDao<T>
    {
        void Inclui(T pEntity);
        void Exclui(T pEntity);
        void Atacha(T pEntity);
        void DesAtacha(T pEntity);
        void Atualiza(T pEntity);
        List<T> AchePorCondicao(Expression<Func<T, bool>> Condicao);
        List<T> PegaTudo();
        void Salva(T pEntity);
        int Qtde();
    }
}
