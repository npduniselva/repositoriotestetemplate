﻿<%@ Page Async="true" Language="C#" AutoEventWireup="true" CodeBehind="NovaInscricao.aspx.cs" Inherits="WebApiNovoCurso.NovaInscricao" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>.:: Nova Inscrição ::.</title>
</head>
<body>
    <form id="form1" method="post" runat="server">
        <div>
            <input type="text" runat="server" id="txtcodturma" /><br />
            <input type="text" runat="server" id="txtnome" /><br />
            <input type="text" runat="server" id="txttelefone" /><br />
            <input type="text" runat="server" id="txtemail" /><br />
            <input type="text" runat="server" id="txtsexo" /><br />
            <input type="text" runat="server" id="txtnascimento" /><br />
            <input type="text" runat="server" id="txtcpf" /><br />
            <input type="text" runat="server" id="txtrg" /><br />
            <input type="text" runat="server" id="txtrgorgao" /><br />
            <input type="text" runat="server" id="txtrguf" /><br />
            <input type="text" runat="server" id="txtrua" /><br />
            <input type="text" runat="server" id="txtnumero" /><br />
            <input type="text" runat="server" id="txtbairro" /><br />
            <input type="text" runat="server" id="txtcep" /><br />
            <input type="text" runat="server" id="txtcidade" /><br />
            <input type="text" runat="server" id="txtuf" /><br />

            <br /><br />

            <asp:LinkButton runat="server" ID="butEnviar" OnClick="butEnviar_Click" CssClass="btn btn-default">Enviar</asp:LinkButton>


        </div>
    </form>
</body>
</html>
